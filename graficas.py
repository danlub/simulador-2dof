import numpy as np
import matplotlib
import matplotlib.pyplot as plt

# Formateo gráficas:
matplotlib.rcParams['text.latex.unicode'] = True
# plt.rc('font', **{'family':'serif', 'serif':['Latin Modern Roman']})
plt.rc('font', **{'family':'serif', 'serif':['Palatino']})
plt.rc('text', usetex=True)
plt.rc('text.latex',preamble=r'\usepackage{color}\usepackage{siunitx}\usepackage{amsmath}\newcommand{\vect}[1]{\mathrm{\mathbf{#1}}}\newcommand{\mat}[1]{\mathsf{#1}}\usepackage[spanish]{babel}\newcommand{\diff}{\mathrm{d}}')
matplotlib.rcParams.update({'font.size': 15})
extFig = ".pdf"
extFig = ".png"
dpinum = 180

dirData = "./"
dirData = "res/dat/"

Yg = np.loadtxt(dirData+"Yg.txt")
Ug = np.loadtxt(dirData+"Ug.txt")
A  = np.loadtxt(dirData+"A.txt")
B  = np.loadtxt(dirData+"B.txt")
KP = np.loadtxt(dirData+"KP.txt")
KD = np.loadtxt(dirData+"KD.txt")
Y  = np.loadtxt(dirData+"Y.txt")
U  = np.loadtxt(dirData+"U.txt")
# Up = np.loadtxt("res/Up.txt")

Y_wp0 = np.loadtxt(dirData+"Y_wp0.txt")
Y_wp  = np.loadtxt(dirData+"Y_wp.txt")

k = 1
DIR = "res/fig/"

alabel = {  "t":    r"Tiempo, [\si{\second}]",
            "v":    r"Velocidad, [\si{\metre\second^{-1}}]",
            "th":   r"Dirección de la velocidad, [\si{\degree}]",
            "h":    r"Altura, [\si{\metre}]",
            "x":    r"Posición horizontal, [\si{\metre}]",
            "T":    r"Empuje, [\si{\kilo\newton}]",
            "d":    r"Dirección del empuje, [\si{\degree}]",
            }

plt.figure(k)
plt.plot(Yg[:,4],Yg[:,3])#,"k-",lw="3.")
plt.plot( Y[:,4], Y[:,3])#,"r--")
plt.scatter(Y_wp0[:,4],Y_wp0[:,3],marker=".")#,s=45.)
plt.scatter(Y_wp[:,4],Y_wp[:,3],marker=".")#,alpha=0.5)
# plt.title("x-h")
plt.xlabel(alabel["x"])
plt.ylabel(alabel["h"])
plt.grid(True)
plt.savefig(DIR+str(k)+"_x-h"+extFig,bbox_inches = 'tight',dpi=dpinum)
k+=1; plt.close('all')

plt.figure(k)
plt.plot(Yg[:,0],Yg[:,1])
plt.plot( Y[:,0], Y[:,1])
# plt.title("t-v")
plt.xlabel(alabel["t"])
plt.ylabel(alabel["v"])
plt.grid(True)
plt.savefig(DIR+str(k)+"_t-v"+extFig,bbox_inches = 'tight',dpi=dpinum)
k+=1; plt.close('all')

plt.figure(k)
plt.plot(Yg[:,0],np.rad2deg(Yg[:,2]))
plt.plot( Y[:,0],np.rad2deg( Y[:,2]))
# plt.title("t-th (deg)")
plt.xlabel(alabel["t"])
plt.ylabel(alabel["th"])
plt.grid(True)
plt.savefig(DIR+str(k)+"_t-th"+extFig,bbox_inches = 'tight',dpi=dpinum)
k+=1; plt.close('all')

plt.figure(k)
plt.plot(Yg[:,0],Yg[:,3])
plt.plot( Y[:,0], Y[:,3])
# plt.title("t-h")
plt.xlabel(alabel["t"])
plt.ylabel(alabel["h"])
plt.grid(True)
plt.savefig(DIR+str(k)+"_t-h"+extFig,bbox_inches = 'tight',dpi=dpinum)
k+=1; plt.close('all')

plt.figure(k)
plt.plot(Yg[:,0],Yg[:,4])
plt.plot( Y[:,0], Y[:,4])
# plt.title("t-x")
plt.xlabel(alabel["t"])
plt.ylabel(alabel["x"])
plt.grid(True)
plt.savefig(DIR+str(k)+"_t-x"+extFig,bbox_inches = 'tight',dpi=dpinum)
k+=1; plt.close('all')

plt.figure(k)
plt.plot(Yg[:,1],Yg[:,3])
plt.plot( Y[:,1], Y[:,3])
# plt.title("h-v")
plt.xlabel(alabel["v"])
plt.ylabel(alabel["h"])
plt.grid(True)
plt.savefig(DIR+str(k)+"_v-h"+extFig,bbox_inches = 'tight',dpi=dpinum)
k+=1; plt.close('all')

plt.figure(k)
plt.plot(Ug[:,0],Ug[:,1]/1e3)
plt.plot( U[:,0], U[:,1]/1e3)
# plt.title("t-T (kN)")
plt.xlabel(alabel["t"])
plt.ylabel(alabel["T"])
plt.grid(True)
plt.savefig(DIR+str(k)+"_t-T"+extFig,bbox_inches = 'tight',dpi=dpinum)
k+=1; plt.close('all')

plt.figure(k)
plt.plot(Ug[:,0],np.rad2deg(Ug[:,2]))
plt.plot( U[:,0],np.rad2deg( U[:,2]))
# plt.title("t-d (deg)")
plt.xlabel(alabel["t"])
plt.ylabel(alabel["d"])
plt.grid(True)
plt.savefig(DIR+str(k)+"_t-d"+extFig,bbox_inches = 'tight',dpi=dpinum)
k+=1; plt.close('all')

exit

plt.figure(k)
plt.plot(A[:,0],A[:,1])
# plt.title("t-A11")
plt.xlabel(alabel["t"])
plt.ylabel(r"$A_{11}$")
plt.grid(True)
plt.savefig(DIR+str(k)+"_t-A11"+extFig,bbox_inches = 'tight',dpi=dpinum)
k+=1; plt.close('all')

plt.figure(k)
plt.plot(A[:,0],A[:,2])
# plt.title("t-A12")
plt.xlabel(alabel["t"])
plt.ylabel(r"$A_{12}$")
plt.grid(True)
plt.savefig(DIR+str(k)+"_t_A12"+extFig,bbox_inches = 'tight',dpi=dpinum)
k+=1; plt.close('all')

plt.figure(k)
plt.plot(A[:,0],A[:,3])
# plt.title("t-A21")
plt.xlabel(alabel["t"])
plt.ylabel(r"$A_{21}$")
plt.grid(True)
plt.savefig(DIR+str(k)+"_t_A21"+extFig,bbox_inches = 'tight',dpi=dpinum)
k+=1; plt.close('all')

plt.figure(k)
plt.plot(A[:,0],A[:,4])
# plt.title("t-A22")
plt.xlabel(alabel["t"])
plt.ylabel(r"$A_{22}$")
plt.grid(True)
plt.savefig(DIR+str(k)+"_t_A22"+extFig,bbox_inches = 'tight',dpi=dpinum)
k+=1; plt.close('all')

plt.figure(k)
plt.plot(B[:,0],B[:,1])
# plt.title("t-B11")
plt.xlabel(alabel["t"])
plt.ylabel(r"$B_{11}$")
plt.grid(True)
plt.savefig(DIR+str(k)+"_t-B11"+extFig,bbox_inches = 'tight',dpi=dpinum)
k+=1; plt.close('all')

plt.figure(k)
plt.plot(B[:,0],B[:,2])
# plt.title("t-B12")
plt.xlabel(alabel["t"])
plt.ylabel(r"$B_{12}$")
plt.grid(True)
plt.savefig(DIR+str(k)+"_t-B12"+extFig,bbox_inches = 'tight',dpi=dpinum)
k+=1; plt.close('all')

plt.figure(k)
plt.plot(B[:,0],B[:,3])
# plt.title("t-B21")
plt.xlabel(alabel["t"])
plt.ylabel(r"$B_{21}$")
plt.grid(True)
plt.savefig(DIR+str(k)+"_t-B21"+extFig,bbox_inches = 'tight',dpi=dpinum)
k+=1; plt.close('all')

plt.figure(k)
plt.plot(B[:,0],B[:,4])
# plt.title("t-B22")
plt.xlabel(alabel["t"])
plt.ylabel(r"$B_{22}$")
plt.grid(True)
plt.savefig(DIR+str(k)+"_t-B22"+extFig,bbox_inches = 'tight',dpi=dpinum)
k+=1; plt.close('all')

plt.figure(k)
plt.plot(KP[:,0],KP[:,1])
# plt.title("t-KP11")
plt.xlabel(alabel["t"])
plt.ylabel(r"$K^P_{11}$")
plt.grid(True)
plt.savefig(DIR+str(k)+"_t-KP11"+extFig,bbox_inches = 'tight',dpi=dpinum)
k+=1; plt.close('all')

plt.figure(k)
plt.plot(KP[:,0],KP[:,2])
# plt.title("t-KP12")
plt.xlabel(alabel["t"])
plt.ylabel(r"$K^P_{12}$")
plt.grid(True)
plt.savefig(DIR+str(k)+"_t-KP12"+extFig,bbox_inches = 'tight',dpi=dpinum)
k+=1; plt.close('all')

plt.figure(k)
plt.plot(KP[:,0],KP[:,3])
# plt.title("t-KP21")
plt.xlabel(alabel["t"])
plt.ylabel(r"$K^P_{21}$")
plt.grid(True)
plt.savefig(DIR+str(k)+"_t-KP21"+extFig,bbox_inches = 'tight',dpi=dpinum)
k+=1; plt.close('all')

plt.figure(k)
plt.plot(KP[:,0],KP[:,4])
# plt.title("t-KP22")
plt.xlabel(alabel["t"])
plt.ylabel(r"$K^P_{22}$")
plt.grid(True)
plt.savefig(DIR+str(k)+"_t-KP22"+extFig,bbox_inches = 'tight',dpi=dpinum)
k+=1; plt.close('all')

plt.figure(k)
plt.plot(KD[:,0],KD[:,1])
# plt.title("t-KD11")
plt.xlabel(alabel["t"])
plt.ylabel(r"$K^D_{11}$")
plt.grid(True)
plt.savefig(DIR+str(k)+"_t-KD11"+extFig,bbox_inches = 'tight',dpi=dpinum)
k+=1; plt.close('all')

plt.figure(k)
plt.plot(KD[:,0],KD[:,2])
# plt.title("t-KD12")
plt.xlabel(alabel["t"])
plt.ylabel(r"$K^D_{12}$")
plt.grid(True)
plt.savefig(DIR+str(k)+"_t-KD12"+extFig,bbox_inches = 'tight',dpi=dpinum)
k+=1; plt.close('all')

plt.figure(k)
plt.plot(KD[:,0],KD[:,3])
# plt.title("t-KD21")
plt.xlabel(alabel["t"])
plt.ylabel(r"$K^D_{21}$")
plt.grid(True)
plt.savefig(DIR+str(k)+"_t-KD21"+extFig,bbox_inches = 'tight',dpi=dpinum)
k+=1; plt.close('all')

plt.figure(k)
plt.plot(KD[:,0],KD[:,4])
# plt.title("t-KD22")
plt.xlabel(alabel["t"])
plt.ylabel(r"$K^D_{22}$")
plt.grid(True)
plt.savefig(DIR+str(k)+"_t-KD22"+extFig,bbox_inches = 'tight',dpi=dpinum)
k+=1; plt.close('all')

#plt.show()


