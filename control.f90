module control

use linalg,     only:   m22inv

implicit none

contains

subroutine calcularMatrizKpKd(A,B,KP,KD,polos,modos)
    real(8), intent(in) :: A(2,2), B(2,2)
    real(8), intent(in) :: polos(2), modos(2,2)
    real(8), intent(out):: KP(2,2), KD(2,2)
    
    real(8), dimension(2,2) :: mpolos, mpolosi, Bi, modosi
    logical :: ok
    
    mpolos = reshape((/polos(1),0d0,0d0,polos(2)/),(/2,2/))
    
    call m22inv(B,Bi,ok)
    
    ! SOLO VALIDO SI A ES DIAGONAL
    ! Si no lo es, hay que resolverse todo el PP
    KP = matmul(Bi,mpolos-A)
    
    call m22inv(mpolos,mpolosi,ok)
    call m22inv(modos ,modosi ,ok)
    
    KD = A + matmul(B, KP)
    KD = matmul(Bi, KD)
    KD = matmul(KD, modos)
    KD = matmul(KD, mpolosi)
    KD = matmul(KD, modosi)
    KD = Bi - KD
    
end subroutine calcularMatrizKpKd

end module control
