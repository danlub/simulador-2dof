module math
implicit none

save

real(8), parameter :: PI = dacos(-1d0)

contains

subroutine RK4(FCN,ti,tf,Yi,Yf,FPAR,NPASOS)
    interface
        function FCN(t,Y,RPAR)
            real(8), intent(in) :: t,Y(:),RPAR(:)
            real(8) :: FCN(size(Y))
        end function FCN
    end interface

    real(8),intent(in) :: ti,Yi(:),FPAR(:)
    integer,intent(in) :: NPASOS
    real(8),intent(inout) :: tf
    real(8),intent(out) :: Yf(:)
    
    integer :: i
    real(8) :: k1(size(Yi)), k2(size(Yi)), k3(size(Yi)), k4(size(Yi))
    real(8) :: tin, dt, Yin(size(Yi))

    dt = (tf - ti)/NPASOS
    tin = ti
    Yin = Yi
    do i = 1,NPASOS
        k1 = FCN(ti          , Yin             , FPAR(:))
        k2 = FCN(ti + 5d-1*dt, Yin + 5d-1*k1*dt, FPAR(:))
        k3 = FCN(ti + 5d-1*dt, Yin + 5d-1*k2*dt, FPAR(:))
        k4 = FCN(ti +      dt, Yin +      k3*dt, FPAR(:))
        tin = tin + dt
        Yin = Yin + (dt/6d0)*(k1 + 2d0*k2 + 2d0*k3 + k4)
    enddo
    tf = tin
    Yf = Yin

end subroutine RK4

! subroutine DP5(FCN,ti,tf,Yi,Yf,FPAR,NPASOS)
!     interface
!         function FCN(t,Y,RPAR)
!             real(8), intent(in) :: t,Y(:),RPAR(:)
!             real(8) :: FCN(size(Y))
!         end function FCN
!     end interface

!     real(8),intent(in) :: ti,Yi(:),FPAR(:)
!     integer,intent(in) :: NPASOS
!     real(8),intent(inout) :: tf
!     real(8),intent(out) :: Yf(:)
    
!     integer :: i
!     real(8) :: k1(size(Yi)), k2(size(Yi)), k3(size(Yi)), k4(size(Yi))
!     real(8) :: tin, dt, Yin(size(Yi))

!     dt = (tf - ti)/NPASOS
!     tin = ti
!     Yin = Yi
!     do i = 1,NPASOS
!         k1 = FCN(ti          , Yin             , FPAR(:))
!         k2 = FCN(ti + 5d-1*dt, Yin + 5d-1*k1*dt, FPAR(:))
!         k3 = FCN(ti + 5d-1*dt, Yin + 5d-1*k2*dt, FPAR(:))
!         k4 = FCN(ti +      dt, Yin +      k3*dt, FPAR(:))
!         tin = tin + dt
!         Yin = Yin + (dt/6d0)*(k1 + 2d0*k2 + 2d0*k3 + k4)
!     enddo
!     tf = tin
!     Yf = Yin

! end subroutine DP5

subroutine linspace(vin, vfn, num, vector)
    real(8),intent(in)  :: vin, vfn
    integer,intent(in) ::num
    real(8),intent(out) :: vector(num)
    integer :: i
    do i = 1,num
        vector(i) = vin + (vfn-vin)*(i-1)/(num-1)
    enddo

end subroutine linspace

subroutine pol2space(vin,vfn,num,vector)
    real(8),intent(in)  :: vin, vfn
    integer,intent(in) ::num
    real(8),intent(out) :: vector(num)
    integer :: i
    call linspace(dsqrt(vin),dsqrt(vfn),num,vector)
    do i = 1,num
        vector(i) = vector(i)**2
    enddo
end subroutine pol2space

subroutine polnspace(vin,vfn,num,vector,n)
    real(8),intent(in)  :: vin, vfn
    integer,intent(in) ::num,n
    real(8),intent(out) :: vector(num)
    integer :: i
    call linspace((vin)**(1d0/n),(vfn)**(1d0/n),num,vector)
    do i = 1,num
        vector(i) = vector(i)**n
    enddo
end subroutine polnspace

FUNCTION cubica (x,coef) result (f)
    implicit none
    real(8), intent(in) :: x, coef(4)
    real(8)             :: f
    
    f = coef(1)*x**3 + coef(2)*x**2 + coef(3)*x + coef(4)
    
end function cubica

!subroutine det22(m,det)
!    real(8),intent(in)  :: m(:,:)
!    real(8),intent(out) :: det

!    det = m(1,1)*m(2,2) - m(1,2)*m(2,1)

!end subroutine det22

!subroutine inversa22 (M, inv, error)
!    real(8), intent(in)  :: M(:,:)
!!     integer, intent(in)  :: dim1, dim2
!    integer, intent(inout) :: error
!    real(8), intent(inout) :: inv(:,:)
!!     integer :: dim
!    real(8) :: det

!!     dim = size(M)/2

!    call det22(M,det)
!!     det = M(1,1)*M(2,2) - M(1,2)*M(2,1)

!    if (abs(det) < 1d-5) then
!        error = -1
!    end if

!    inv(1,1) = (+M(2,2))/det
!    inv(1,2) = (-M(2,1))/det
!    inv(2,1) = (-M(1,2))/det
!    inv(2,2) = (+M(1,1))/det

!end subroutine inversa22

!subroutine det33(m,det)
!    real(8),intent(in)  :: m
!    real(8),intent(out) :: det
    
!    det = m(1,1)*m(2,2)*m(3,3) + m(1,2)*m(2,3)*m(3,1) + m(1,3)*m(3,2)*m(2,1)
!    det = det - m(1,1)*m(2,3)*m(3,2) - m(1,2)*m(2,1)*m(3,3) - m(1,3)*m(3,1)*m(2,2)

!end subroutine det33

!subroutine inversa33(m, inv, error)
!    REAL(8),intent(in)  :: m(:,:)
!    LOGICAL,INTENT(INOUT) :: error
!    REAL(8),intent(out) ::  inv(:,:)

!    real(8) :: det

!    call det33(m,det)

!    inv(1,1) = + m(1,1) * ( m(2,2)*m(3,3) - m(2,3)*m(3,2) ) / det
!    inv(1,2) = - m(1,2) * ( m(2,1)*m(3,3) - m(2,3)*m(3,1) ) / det
!    inv(1,3) = + m(1,3) * ( m(2,1)*m(3,2) - m(2,2)*m(3,1) ) / det

!    inv(2,1) = - m(2,1) * ( m(1,2)*m(3,3) - m(1,3)*m(3,2) ) / det
!    inv(2,2) = + m(2,2) * ( m(1,1)*m(3,3) - m(1,3)*m(3,1) ) / det
!    inv(2,3) = - m(2,3) * ( m(1,1)*m(3,2) - m(1,2)*m(3,1) ) / det

!    inv(3,1) = + m(3,1) * ( m(1,2)*m(2,3) - m(2,2)*m(1,3) ) / det
!    inv(3,2) = - m(3,2) * ( m(1,1)*m(2,3) - m(2,1)*m(1,3) ) / det
!    inv(3,3) = + m(3,3) * ( m(1,1)*m(2,2) - m(1,2)*m(2,1) ) / det

!end subroutine inversa33

end module math
