module tierra
implicit none

contains

subroutine modelo_gravedad(altura,g)
    real(8),intent(in)  :: altura
    real(8),intent(out) :: g
    real(8) :: mu, R_T
    !TODO_add_body
    mu = 3.986004418d+14
    R_T = 6375416.323689184d0
    g = mu / (R_T + altura)**2
end subroutine modelo_gravedad

subroutine atmosisa(altura,temp,velson,p,rho)
    ! Implementacion de la International Standard Atmosphera
    ! Valida solo hasta 11 km
    real(8), intent(in) :: altura
    real(8), intent(out) :: temp, velson, p, rho

    real(8) :: a, temp0, p0, h0, R, g0, rho0, gamma

    a = -6.5d0
    temp0 = 288.15d0
    p0 = 101325d0
    h0 = 0d0
    R = 287d0
    g0 = 9.80665d0
    rho0 = 1.225d0
    gamma = 1.4d0
    temp = temp0 + a *(altura - h0)/1000d0
    p = p0 * (temp/temp0)**(-g0/(a/1000d0*R))
    rho = rho0*(temp/temp0)**(-g0/(a/1000d0*R)-1d0)
    velson = dsqrt(temp*gamma*R)
end subroutine atmosisa

subroutine atmosisa_temp(h,temp)
    real(8),intent(in)  :: h
    real(8),intent(out) :: temp
    real(8) :: d1,d2,d3
    !TODO_add_body
    call atmosisa(h,temp,d1,d2,d3)
end subroutine atmosisa_temp

subroutine atmosisa_velson(h,velson)
    real(8),intent(in)  :: h
    real(8),intent(out) :: velson
    real(8) :: d1,d2,d3
    !TODO_add_body
    call atmosisa(h,d1,velson,d2,d3)
end subroutine atmosisa_velson

subroutine atmosisa_p(h,p)
    real(8),intent(in)  :: h
    real(8),intent(out) :: p
    real(8) :: d1,d2,d3
    !TODO_add_body
    call atmosisa(h,d1,d2,p,d3)
end subroutine atmosisa_p

subroutine atmosisa_rho(h,rho)
    real(8),intent(in)  :: h
    real(8),intent(out) :: rho
    real(8) :: d1,d2,d3
    !TODO_add_body
    call atmosisa(h,d1,d2,d3,rho)
end subroutine atmosisa_rho

end module tierra